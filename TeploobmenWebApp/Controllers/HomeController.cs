﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using TeploLibrary;
using TeploobmenWebApp.Models;

namespace TeploobmenWebApp.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        public IActionResult Index(TeploobmenInput input)
        {
            var lib = new TeploLib(input);
            var result = lib.Res();

            return View(result);

        }
        [HttpGet]
        public IActionResult Index(int? variantId)
        {
          
            return View();
        }

    }
}