﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeploLibrary
{
    public class TeploobmenOutput
    {
        public List<TeploobmenOutputRow>? Rows { get; set; }
    }

    public class TeploobmenOutputRow
    {
        public double SloyH { get; set; }
        public double Y { get; set; }
        public double Vt { get; set; }
        public double Qt { get; set; }
        public double V { get; set; }
        public double Q { get; set; }
        public double t { get; set; }
        public double T { get; set; }
        public double Razn { get; set; }

    }
}
