﻿namespace TeploLibrary
{
    public class TeploLib
    {
        public double H { get; set; }
        public double NTM { get; set; }
        public double NTG { get; set; }
        public double VG { get; set; }
        public double TG { get; set; }
        public double RM { get; set; }
        public double TM { get; set; }
        public double OKT { get; set; }
        public double D { get; set; }

        public TeploLib(TeploobmenInput input)
        {
            H = input.H;
            NTM = input.NTM;
            NTG = input.NTG;
            VG = input.VG;
            TG = input.TG;
            RM = input.RM;
            TM = input.TM;
            OKT = input.OKT;
            D = input.D;
        }
        // Функция возвращающая результат
        public TeploobmenOutput Res()
        {
            double[] sloy = new double[(int)(H / 0.5 + 1)];
            double HSl = 0;
            for (int i = 0; i < (int)(H / 0.5 + 1); i++)
            {
                sloy[i] = HSl;
                HSl += 0.5;
            }

            var model = new TeploobmenOutput()
            {
                Rows = new List<TeploobmenOutputRow>()
            };

            double m = TM * RM / (VG * TG * Math.PI * (D / 2) * (D / 2));
            double yo = OKT * H / (VG * TG * 1000);

            foreach (var y in sloy)
            {
                var row = new TeploobmenOutputRow
                {
                    SloyH = y,
                    Y = OKT * y / (VG * TG * 1000),
                    Vt = 1 - Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m),
                    Qt = 1 - m * Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m),
                    V = (1 - Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m)) / (1 - m * Math.Exp((m - 1) * (OKT * yo / (VG * TG * 1000)) / m)),
                    Q = (1 - m * Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m)) / (1 - m * Math.Exp((m - 1) * (OKT * yo / (VG * TG * 1000)) / m)),
                    t = NTM + (TG - NTM) * ((1 - Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m)) / (1 - m * Math.Exp((m - 1) * (OKT * yo / (VG * TG * 1000)) / m))),
                    T = NTM + (TG - NTM) * ((1 - m * Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m)) / (1 - m * Math.Exp((m - 1) * (OKT * yo / (VG * TG * 1000)) / m))),
                    Razn = NTM + (TG - NTM) * ((1 - Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m)) / (1 - m * Math.Exp((m - 1) * (OKT * yo / (VG * TG * 1000)) / m))) - (NTM + (TG - NTM) * ((1 - m * Math.Exp((m - 1) * (OKT * y / (VG * TG * 1000)) / m)) / (1 - m * Math.Exp((m - 1) * (OKT * yo / (VG * TG * 1000)) / m))))
                };
                model.Rows.Add(row);
            }
            return model;
        }

    }
}
