﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeploLibrary
{
    public class TeploobmenInput
    {
        public double H { get; set; }
        public double NTM { get; set; }
        public double NTG { get; set; }
        public double VG { get; set; }
        public double TG { get; set; }
        public double RM { get; set; }
        public double TM { get; set; }
        public double OKT { get; set; }
        public double D { get; set; }
        public string? Name { get; set; }
    }
}


